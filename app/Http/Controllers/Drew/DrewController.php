<?php

namespace App\Http\Controllers\Drew;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Drewlogic\V1\Drew;

class DrewController extends Controller
{
    var $drew; // 抽卡 class

    function __construct(){
        $this -> drew = new Drew;
    }

    /**
     * [查看機率]
     * @param  Request $request [傳入值]
     * @return [array]          [預設100組獎項]
     */
    function drewAwardProbability(Request $request){
        $this -> drew -> setInfo($request -> all());
        $ary = $this -> drew -> drewAwardProbability();
        dd($ary);
    }

    /**
     * [查看獎項]
     * @param  Request $request [傳入值]
     * @return [array]          [1~11組獎項]
     */
    function getDrewStarAry(Request $request){
        $this -> drew -> setInfo($request -> all());
        $ary = $this -> drew -> getDrewStarAry();
        return json_encode($ary);
    }
}
