<?php

namespace App\Drewlogic\V1;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\V1\DrewRepositories;

class Drew extends Model
{
    var $precent; // N分之一，預設100
    var $star; // 獎項
    var $specialProbability; // 特定族群機率上升
    var $mustGetRedCard; // 必中紅卡 預設 false
    var $probability; // 機率
    var $drewTimesGetRedCard; // 抽幾次必中紅卡
    var $specialDrewTimesGetRedCard; // 抽幾次必中特定紅卡

    var $activate; // 活動抽卡是否有限定 預設 false
    var $drewClassType; // 單抽 or 10 + 1

    protected $idxAry = []; // 索引值，防重複
    protected $drewStarAry = []; // 返回索引位置
    protected $cardPool = []; // 所有卡池
    protected $specialPoolType; // 特殊持指定屬性類別
    protected $DB; // 資料庫

    var $drewTimes; // 第幾抽
    var $poolTarget; // 抽目標卡池 預設友情抽

    function __construct(){
        // 獎項
        $this -> star = [4, 5];
        // 機率
        $this -> probability = [88.238, 11.762];
        // N分之一，預設100
        $this -> precent = 100;
        // 單抽 or 10 + 1
        $this -> drewClassType = 1;
        // 第幾抽
        $this -> drewTimes = 1;
        // 抽目標卡池 預設友情抽
        $this -> poolTarget = 99;
        // 活動抽卡是否有限定 預設 false
        $this -> activate = false;
        // 活動抽卡 10 + 1 幾次最後一張是否必中限定 預設 3 次
        $this -> drewTimesGetRedCard = 3;
        // 活動抽卡 10 + 1 幾次最後一張是否必中限定 預設 3 次
        $this -> specialDrewTimesGetRedCard = 3;
        // 活動抽卡最後一張是否必中限定 預設 false
        $this -> mustGetRedCard = false;
        // 活動抽卡最後一張是否必中特定限定 預設 false
        $this -> specialProbability = false;
        // 特殊持指定屬性類別
        $this -> specialPoolType = "3, 4, 5";
        // DB init
        $this -> DB = new DrewRepositories;
    }

    /**
     * [setInfo 設定抽獎參數]
     * @param [array] $setting [傳入參數值]
     */
    function setInfo($setting){
        // N分之一，預設100
        // $this -> precent = 100;
        // 單抽 or 10 + 1
        $this -> drewClassType = $setting['drewClassType'] ?? $this -> drewClassType;
        // 第幾抽
        $this -> drewTimes = $setting['drewTimes'] ?? $this -> drewTimes;
        // 抽目標卡池 預設友情抽
        $this -> poolTarget = $setting['poolTarget'] ?? $this -> poolTarget;
        // 活動抽卡是否有限定 預設 false
        $this -> activate = true;
        // 活動抽卡 10 + 1 幾次最後一張是否必中限定 預設 3 次
        $this -> drewTimesGetRedCard = 2;
        // 活動抽卡 10 + 1 幾次最後一張是否必中特定限定 預設 3 次
        $this -> specialDrewTimesGetRedCard = 3;
        // 活動抽卡最後一張是否必中限定 預設 false
        $this -> mustGetRedCard = true;
        // 活動抽卡最後一張是否必中特定限定 預設 false
        $this -> specialProbability = true;
        // 特殊持指定屬性類別
        $this -> specialPoolType = "3, 4, 5";

        $this -> costMoneyPoolSetting();
        switch ($this -> poolTarget) {
            case 2:
                $this -> specialPool();
                break;

            case 99:
                $this -> friendPoolSetting();
                break;

            default:
                # code...
                break;
        }
    }

    /**
     * [friendPoolSetting 友情池]
     */
    function friendPoolSetting(){
        // 獎項
        $this -> star = [1, 2, 3, 4, 5, 6];
        // 機率
        $this -> probability = [10, 30, 30, 20, 7, 3];
    }

    /**
     * [costMoneyPoolSetting 錢卡池，預設不含限定必四星以上]
     * @param [array] $star [陣列裡有 6 必為限定]
     */
    function costMoneyPoolSetting(){
        // 單抽必定三星以上(含限定)
        if($this -> drewClassType == 1 && $this -> activate){
            $this -> star = [3, 4, 5, 6];
            $this -> probability = [20, 71, 6, 3];
        }
        // 單抽必定三星以上(不含限定)
        if($this -> drewClassType == 1 && !$this -> activate){
            $this -> star = [3, 4, 5];
            $this -> probability = [20, 73.214, 6.786];
        }
        // 10 + 1 抽 必定四星以上(含限定)
        if($this -> drewClassType > 1 && $this -> activate){
            // 獎項
            $this -> star = [4, 5, 6];
            // 機率
            $this -> probability = [88.238, 8.762, 3];
        }
        if($this -> specialProbability)
            $this -> specialCostMoneyPoolSetting();
    }

    /**
     * [specialCostMoneyPoolSetting 錢卡池，預設不含限定必四星以上]
     * @param [array] $star [陣列裡有 6 必為限定]
     */
    function specialCostMoneyPoolSetting(){
        // 獎項
        $this -> star = [4, 5, 6, 97, 98, 99];
        // 機率
        $this -> probability = [84.238, 8.762, 3, 2, 1, 1];
    }

    function specialPool(){
        // 獎項
        $this -> star = [3, 4, 5, 7, 8];
        // 機率
        $this -> probability = [20, 70.214, 4.786, 3, 1];
    }

    /**
     * 抽獎機率
     * @return [array] [返回得獎陣列]
     */
    function drewAwardProbability(){
        // 防止重複
        while (count($this -> idxAry) != $this -> precent) {
            $idx = mt_rand(0, $this -> precent - 1);
            if(!in_array($idx, $this -> idxAry))
                $this -> idxAry[] = $idx;
        }
        $outAry = [];
        // 輸出獎項
        for ($i = 0; $i < count($this -> star); $i++) {
            for ($j = 0; $j < $this -> probability[$i]; $j++) {
                $outAry[$this -> idxAry[$j]] = $this -> star[$i];
            }
        }
        // 整理陣列
        ksort($outAry);
        // 清空
        unset($this -> idxAry);
        return $outAry;
    }

    /**
     * 獎項
     * @return [array] [返回獎項陣列]
     */
    function getDrewStarAry(){
        $outAry = $this -> drewAwardProbability();
        if($this -> drewClassType > 1){
            foreach (array_rand($outAry, $this -> drewClassType) as $v) {
                $this -> drewStarAry[] = $outAry[$v];
            }
        }else{
            $this -> drewStarAry[] = $outAry[array_rand($outAry, $this -> drewClassType)];
        }
        if($this -> poolTarget == 1 && $this -> mustGetRedCard)
            $this -> drewRedCardRule();
        // return $this -> drewStarAry;
        return $this -> cardPool();
    }

    // 必中紅卡規則
    function drewRedCardRule(){
        if($this -> drewTimes % $this -> drewTimesGetRedCard == 0)
            $this -> finalMustGetRedCard();
        if($this -> drewTimes % $this -> specialDrewTimesGetRedCard == 0 && $this -> specialProbability)
            $this -> finalMustGetTargetRedCard();
    }

    // 最後一張卡必為限定
    protected function finalMustGetRedCard(){
        unset($this -> drewStarAry[10]);
        $this -> drewStarAry[10] = 6;
    }

    // 最後一張卡必為特定限定
    protected function finalMustGetTargetRedCard(){
        unset($this -> drewStarAry[10]);
        $this -> drewStarAry[10] = 99;
    }

    /**
     * 抽到卡片的陣列
     * @return [array] [數字陣列]
     */
    protected function cardPool(){
        try{
            $outAry = [];
            foreach($this -> drewStarAry as $star){
                $cardNum = $this -> cardID($star);
                $outAry[] = $cardNum;
            }
        }catch(Exception $e){
            $this -> thownError();
        }
        if($this -> debug)var_dump($outAry);
        return $outAry;
    }

    /**
     * 抽到什麼卡
     * @param  [int] $star [幾星]
     * @return [int]       [卡片ID]
     */
    protected function cardID($star){
        // 此處為資料庫連接取得怪物資料，若有資料庫的話...恩
        // 須帶入卡池目標 poolTarget
        // $this -> cardPool = [];
        $where = "";
        // if($this -> poolTarget != 1 || $this -> poolTarget != 99)
        //     $where .= "in({$this -> specialPoolType})";
        switch ($star){
            case 1:
                $this -> cardPool = [
                    ["cardId" => 14],
                    ["cardId" => 16],
                    ["cardId" => 18]
                ];
                break;
            case 2:
                $this -> cardPool = [
                    ["cardId" => 15],
                    ["cardId" => 17],
                    ["cardId" => 19]
                ];
                break;
            case 3:
                $this -> cardPool = [
                    ["cardId" => 1],
                    ["cardId" => 4],
                    ["cardId" => 7]
                ];
                if($this -> poolTarget == 99)
                    $this -> cardPool = [
                        ["cardId" => 28],
                        ["cardId" => 29],
                        ["cardId" => 30]
                    ];
                break;
            case 4:
                $this -> cardPool = [
                    ["cardId" => 2],
                    ["cardId" => 5],
                    ["cardId" => 8]
                ];
                if($this -> poolTarget == 99)
                    $this -> cardPool = [
                        ["cardId" => 25],
                        ["cardId" => 26],
                        ["cardId" => 27]
                    ];
                break;
            case 5:
                $this -> cardPool = [
                    ["cardId" => 3],
                    ["cardId" => 6],
                    ["cardId" => 9]
                ];
                if($this -> poolTarget == 99)
                    $this -> cardPool = [
                        ["cardId" => 22],
                        ["cardId" => 23],
                        ["cardId" => 24]
                    ];
                break;
            case 6:
                $this -> cardPool = [
                    ["cardId" => 10],
                    ["cardId" => 12],
                    ["cardId" => 39],
                    ["cardId" => 41],
                    ["cardId" => 43],
                    ["cardId" => 45],
                ];
                if($this -> poolTarget == 99)
                    $this -> cardPool = [
                        ["cardId" => 20],
                        ["cardId" => 21]
                    ];
                break;
            // 6 以後為特殊屬性限定
            case 7:
                if($this -> poolTarget == 2){
                    $this -> cardPool[] = ["cardID" => 31];
                    $this -> cardPool[] = ["cardID" => 33];
                }
                if($this -> poolTarget == 3){
                    $this -> cardPool[] = ["cardID" => 35];
                    $this -> cardPool[] = ["cardID" => 37];
                }
                break;
            case 8:
                if($this -> poolTarget == 2){
                    $this -> cardPool[] = ["cardID" => 32];
                    $this -> cardPool[] = ["cardID" => 34];
                }
                if($this -> poolTarget == 3){
                    $this -> cardPool[] = ["cardID" => 36];
                    $this -> cardPool[] = ["cardID" => 38];
                }
                break;
            case 97:
                $this -> cardPool = [
                    ["cardID" => 5],
                    ["cardID" => 2]
                ];
                break;
            case 98:
                $this -> cardPool = [
                    ["cardID" => 6],
                    ["cardID" => 3]
                ];
                break;
            case 99:
                $this -> cardPool = [
                    ["cardID" => 10],
                    ["cardId" => 39],
                    ["cardId" => 43],
                ];
                break;
        }
        return $this -> cardPool[mt_rand(0, count($this -> cardPool) - 1)];
    }
}
